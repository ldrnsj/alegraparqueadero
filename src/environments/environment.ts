// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase  :{
    apiKey: "AIzaSyA3qff7kkBXmZBRtZ4Y8Bd_q_gGHTW8Yb8",
    authDomain: "alegrapark.firebaseapp.com",
    databaseURL: "https://alegrapark.firebaseio.com",
    projectId: "alegrapark",
    storageBucket: "alegrapark.appspot.com",
    messagingSenderId: "751051583959"
  },
  token:"Basic "+btoa("alejoo1894@gmail.com:2454830df91b77124290") ,
  url:"https://app.alegra.com/api/v1/invoices"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
