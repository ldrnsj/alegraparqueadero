import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreDocument
} from "angularfire2/firestore";
import { Observable } from "rxjs";
import { Puestos } from "../../interface/puestos";
@Injectable({
  providedIn: "root"
})
export class FirestoreService {
  constructor(private firestore: AngularFirestore) {}
  public getSPuesto(id: string) {
    return this.firestore
      .collection("Puestos")
      .doc(id)
      .snapshotChanges();
  }
  public getPuesto() {
    return this.firestore.collection("Puestos").snapshotChanges();
  }
  public updatePuesto(id: string, data: any, option) {
    let dateUnix = new Date().getTime();
    let tAcumulado = (dateUnix - data.Hora) / 1000;
    data.Acarreado = data.Acarreado + Math.trunc(tAcumulado);

    if (option == 0) {
      return this.firestore
        .collection("Puestos")
        .doc(id)
        .set(data);
    } else {
      data.Cantidad = data.Cantidad + 1;
      data.Hora = 0;
      data.Marca = "0";
      data.Ocupado = false;
      data.Placa = "0";
      data.Inicial="x";
      return this.firestore
        .collection("Puestos")
        .doc(id)
        .set(data);
    }
  }
}
