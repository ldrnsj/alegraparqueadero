import { TestBed } from '@angular/core/testing';

import { ParkServicesService } from './park-services.service';
import {} from 'jasmine';

describe('ParkServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParkServicesService = TestBed.get(ParkServicesService);
    expect(service).toBeTruthy();
  });
});
