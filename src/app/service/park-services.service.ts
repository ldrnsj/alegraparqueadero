import { Injectable, Inject, Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import * as moment from "moment";
import { environment } from "../../environments/environment";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Injectable({
  providedIn: "root"
})
export class ParkServicesService {
  constructor(private http: HttpClient, public dialog: MatDialog) {}
  moment = require("moment");

  public generarFactura(body: any) {
    let timePark = body.data.Hora;
    let dateUnix = new Date().getTime();
    let date = this.getDate(dateUnix);
    let tasar = Math.trunc((dateUnix - timePark) / 1000);
    let price = Math.trunc(tasar) * 30;
    if (price <= 0) {
      price = 30;
    }

    let params: any = {
      date: date,
      dueDate: date,
      client: 1,
      items: [
        {
          id: 1,
          price: price,
          quantity: 1
        }
      ],
      anotation: `Vehiculo de marca ${body.data.Marca} con placa ${
        body.data.Placa
      } en el puesto de estacionamiento ${body.id}`,
      payments: [{ date: date, account: 1, amount: price }]
    };
    this.http
      .post(environment.url, params, {
        headers: { Authorization: environment.token }
      })
      .subscribe(result => {});
  }

  getDate(fecha) {
    let d = new Date(fecha);
    let day = d.getDate();
    let month = d.getMonth() + 1;
    let year = d.getFullYear();
    return `${year}-${month}-${day}`;
  }
}
