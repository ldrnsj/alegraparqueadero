import { BrowserModule } from "@angular/platform-browser";
import { NgModule, LOCALE_ID } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { MaterialModule } from "./material/material.module";
import { AngularFireModule } from "angularfire2";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { ParkComponent, top5,Confirmar } from "./park/park.component";
import { environment } from "../environments/environment";
import { ParkServicesService } from "./service/park-services.service";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { MatNativeDateModule } from "@angular/material";
import {
  AngularFirestore,
  AngularFirestoreDocument
} from "angularfire2/firestore";

@NgModule({
  declarations: [AppComponent, HomeComponent, ParkComponent, top5,Confirmar],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    HttpClientModule,
    MaterialModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    ReactiveFormsModule
  ],
  entryComponents: [top5, ParkComponent,Confirmar],
  providers: [
    AngularFirestore,
    ParkServicesService,
    { provide: LOCALE_ID, useValue: "es-VE" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
