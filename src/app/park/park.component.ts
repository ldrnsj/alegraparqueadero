import { Component, OnInit, OnChanges, Inject } from "@angular/core";
import { AngularFireDatabase, AngularFireObject } from "@angular/fire/database";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FirestoreService } from "../service/firestore/firestore.service";
import { ParkServicesService } from "../service/park-services.service";

@Component({
  selector: "app-park",
  templateUrl: "./park.component.html",
  styleUrls: ["./park.component.css"]
})
export class ParkComponent implements OnInit {
  public puesto = [];
  public puestoLibre = [];
  Front: any = [];
  Back: any = [];
  cambio: boolean = false;
  vehiculo = [];
  public marca;
  public placa;
  public esconder: boolean = false;
  public time = new Date().getTime();

  constructor(
    private firestoreService: FirestoreService,
    private servPar: ParkServicesService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.firestoreService.getPuesto().subscribe(snap => {
      this.puesto = [];
      snap.forEach((data: any) => {
        this.puesto.push({
          id: data.payload.doc.id,
          data: data.payload.doc.data()
        });
      });
    });
    console.log(this.puesto);
  }

  topPuestos() {
    this.esconder = true;
    const modal = this.dialog.open(top5, {
      data: this.puesto
    });

    modal.afterClosed().subscribe(result => {
      this.esconder = false;
      this.puesto.sort(function(obj1, obj2) {
        return obj1.id - obj2.id;
      });
    });
  }

  buscarP() {
    if (
      this.placa == null ||
      this.marca == null ||
      this.placa == "" ||
      this.marca == ""
    ) {
      alert("Debe ingresar placa y marca del vehiculo a parquear");
    } else {
      this.vehiculo = [];
      this.vehiculo.push(this.marca);
      this.vehiculo.push(this.placa);
      this.vehiculo.push();
      this.buscarPuesto(this.vehiculo, 0);
    }
  }
  buscarPuesto(vehiculo: any, reubicar) {
    this.Front = [];
    this.Back = [];

    this.puestoVacio();
    this.puestoLibre.forEach(puestosGenerales => {
      if (
        puestosGenerales.id == 1 ||
        puestosGenerales.id == 2 ||
        puestosGenerales.id == 3 ||
        puestosGenerales.id == 4 ||
        puestosGenerales.id == 0
      ) {
        this.Front.push(puestosGenerales);
      } else {
        this.Back.push(puestosGenerales);
      }
    });

    let Ids = [];
    let cantidad: any;
    let acarreado: any;

    if (this.Front.length > 0) {
      this.Front.forEach(element => {
        Ids.push(element.id);
      });
      let random = Ids[Math.floor(Math.random() * Ids.length)];

      this.puesto.forEach(element => {
        if (element.id == random) {
          cantidad = element.data.Cantidad;
          acarreado = element.data.Acarreado;
        }
      });

      if (reubicar == 0) {
        let dataService: any = {};
        dataService.Marca = vehiculo[0];
        dataService.Placa = vehiculo[1];
        dataService.Cantidad = cantidad;
        dataService.Hora = new Date().getTime();
        dataService.Ocupado = true;
        dataService.Acarreado = acarreado;
        dataService.Inicial = "x";
        this.firestoreService.updatePuesto(random, dataService, 0);
      } else {
        let dataService: any = {};
        dataService.Marca = vehiculo.data.Marca;
        dataService.Placa = vehiculo.data.Placa;
        dataService.Cantidad = cantidad;
        dataService.Hora = vehiculo.data.Hora;
        dataService.Ocupado = true;
        dataService.Acarreado = acarreado;
        dataService.Inicial = vehiculo.id;
        //ldrnsj

        this.firestoreService.updatePuesto(random, dataService, 0);
      }
    } else if (this.Back.length > 0) {
      this.Back.forEach(element => {
        Ids.push(element.id);
      });

      let random = Ids[Math.floor(Math.random() * Ids.length)];
      this.puesto.forEach(element => {
        if (element.id == random) {
          cantidad = element.data.Cantidad;
          acarreado = element.data.Acarreado;
        }
      });

      if (reubicar == 0) {
        let dataService: any = {};
        dataService.Marca = vehiculo[0];
        dataService.Placa = vehiculo[1];
        dataService.Cantidad = cantidad;
        dataService.Hora = new Date().getTime();
        dataService.Ocupado = true;
        dataService.Acarreado = acarreado;
        dataService.Inicial = "x";


        this.firestoreService.updatePuesto(random, dataService, 0);
      } else {
        let dataService: any = {};
        dataService.Marca = vehiculo.data.Marca;
        dataService.Placa = vehiculo.data.Placa;
        dataService.Cantidad = cantidad;
        dataService.Hora = new Date().getTime();
        dataService.Ocupado = true;
        dataService.Acarreado = acarreado;
        dataService.Inicial = vehiculo.id;


        this.firestoreService.updatePuesto(random, dataService, 0);
      }
    } else {
      alert(
        "El estacionamiento se encuentra lleno, por favor esperar a la salida de alguno"
      );
    }
  }

  puestoVacio() {
    this.puestoLibre = [];
    this.puesto.filter(puesto => {
      if (!puesto.data.Ocupado) {
        this.puestoLibre.push(puesto);
      }
    });
  }

  checkout(id: string, data: any) {
    this.cambio = false;

    let modalData = [];
    let timePark = data.Hora;
    let dateUnix = new Date().getTime();
    let tasar = Math.trunc((dateUnix - timePark) / 1000);
    let price = Math.trunc(tasar) * 30;
    modalData.push({ data: data, price: price, tasar: tasar ,id:id});
    const modal = this.dialog.open(Confirmar, { data: modalData });
    modal.afterClosed().subscribe(result => {
      if (result == 1) {
        switch (id) {
          case "0":
            this.sacarDelPuesto(id, data);
            this.trancado(5);
            if (this.cambio) {
              this.puesto.forEach(element => {
                if (element.id == 5) {
                  this.buscarPuesto(element, 1);
                  this.firestoreService.updatePuesto(
                    element.id,
                    element.data,
                    1
                  );
                }
              });
            }
            break;
          case "1":
            this.sacarDelPuesto(id, data);
            this.trancado(6);
            if (this.cambio) {
              this.puesto.forEach(element => {
                if (element.id == 6) {
                  this.buscarPuesto(element, 1);
                  this.firestoreService.updatePuesto(
                    element.id,
                    element.data,
                    1
                  );
                }
              });
            }

            break;
          case "2":
            this.sacarDelPuesto(id, data);
            this.trancado(7);
            if (this.cambio) {
              this.puesto.forEach(element => {
                if (element.id == 7) {
                  this.buscarPuesto(element, 1);
                  this.firestoreService.updatePuesto(
                    element.id,
                    element.data,
                    1
                  );
                }
              });
            }

            break;
          case "3":
            this.sacarDelPuesto(id, data);
            this.trancado(8);
            if (this.cambio) {
              this.puesto.forEach(element => {
                if (element.id == 8) {
                  this.buscarPuesto(element, 1);
                  this.firestoreService.updatePuesto(
                    element.id,
                    element.data,
                    1
                  );
                }
              });
            }

            break;
          case "4":
            this.sacarDelPuesto(id, data);
            this.trancado(9);
            if (this.cambio) {
              this.puesto.forEach(element => {
                if (element.id == 9) {
                  this.buscarPuesto(element, 1);
                  this.firestoreService.updatePuesto(
                    element.id,
                    element.data,
                    1
                  );
                }
              });
            }

            break;

          default:
            this.sacarDelPuesto(id, data);

            break;
        }
      }
    });
  }

  sacarDelPuesto(id: string, data: any) {
    let body: any = { id, data };
    this.servPar.generarFactura(body);
    this.firestoreService.updatePuesto(id, data, 1);
  }

  public trancado(id) {
    this.puesto.forEach(element => {
      if (element.id == id && element.data.Ocupado) {
        this.cambio = true;
      }
    });
  }
}

@Component({
  selector: "app-top5",
  templateUrl: "./top5.component.html"
})
export class top5 {
  puestoTop: any;

  constructor(
    public dialog: MatDialogRef<top5>,
    @Inject(MAT_DIALOG_DATA) public datos: any
  ) {
    this.puestoTop = this.datos;
    this.puestoTop.sort(function(obj1, obj2) {
      return obj2.data.Cantidad - obj1.data.Cantidad;
    });
    Math.floor(2);
  }

  ok() {
    this.dialog.close();
  }

  cerrar() {
    this.dialog.close();
  }
}

@Component({
  selector: "confirmar",
  templateUrl: "./confirmar.component.html"
})
export class Confirmar {
  constructor(
    public dialog: MatDialogRef<Confirmar>,
    @Inject(MAT_DIALOG_DATA) public datos: any
  ) {
  }

  cerrar() {
    debugger;
    this.dialog.close();
  }
}
