import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ParkComponent } from './park/park.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'park', component: ParkComponent },
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: '**', redirectTo: '' },
  { path: 'redirectToHome', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', redirectTo: '/404' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
